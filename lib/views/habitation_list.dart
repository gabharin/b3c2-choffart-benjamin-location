import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:location/models/habitation.dart';
import 'package:location/services/habitation_service.dart';
import 'package:location/views/habitation_details.dart';
import 'package:location/views/shared/habitation_features_widget.dart';

class HabitationList extends StatelessWidget {
  final HabitationService habitationService = HabitationService();
  final bool isHouseList;
  late List<Habitation> _habitations;

  HabitationList(this.isHouseList, {super.key}) {
    _habitations = isHouseList
        ? habitationService.getHouses()
        : habitationService.getApartments();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Liste des ${isHouseList ? 'maisons' : 'appartements'}"),
      ),
      body: Center(
          child: ListView.builder(
              itemCount: _habitations.length,
              itemBuilder: (context, index) =>
                  _buildRow(_habitations[index], context),
              itemExtent: 280)),
    );
  }

  _buildRow(Habitation habitation, BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(4.0),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => HabitationDetails(habitation)));
        },
        child: Column(
          children: [
            SizedBox(
              height: 150,
              width: MediaQuery.of(context).size.width,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: Image.asset(
                  'assets/images/locations/${habitation.image}',
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
            _buildHabitationDetails(habitation)
          ],
        ),
      ),
    );
  }

  _buildHabitationDetails(Habitation habitation) {
    final format = NumberFormat("### €");
    return Container(
      child: Column(children: [
        Row(
          children: [
            Expanded(
              flex: 3,
              child: ListTile(
                title: Text(habitation.name),
                subtitle: Text(habitation.address),
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(
                format.format(habitation.pricePerMonth),
                style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Roboto',
                    fontSize: 22),
              ),
            )
          ],
        ),
        HabitationFeaturesWidget(habitation)
      ]),
    );
  }
}
