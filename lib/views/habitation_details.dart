import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:location/models/habitation.dart';
import 'package:location/shared/location_style.dart';
import 'package:location/shared/location_text_style.dart';
import 'package:location/views/book_location.dart';
import 'package:location/views/shared/habitation_features_widget.dart';

class HabitationDetails extends StatefulWidget {
  final Habitation _habitation;

  const HabitationDetails(this._habitation, {super.key});

  @override
  State<HabitationDetails> createState() => _HabitationDetailsState();
}

class _HabitationDetailsState extends State<HabitationDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget._habitation.name),
      ),
      body: ListView(
        padding: const EdgeInsets.all(4.0),
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Image.asset(
              'assets/images/locations/${widget._habitation.image}',
              fit: BoxFit.fitWidth,
            ),
          ),
          Container(
            margin: const EdgeInsets.all(8.0),
            child: Text(widget._habitation.address),
          ),
          HabitationFeaturesWidget(widget._habitation),
          _buildCustomDivider("Inclus"),
          widget._habitation.options.isNotEmpty
              ? _buildItems()
              : _buildNoItems(false),
          _buildCustomDivider("Options"),
          widget._habitation.paidOptions.isNotEmpty
              ? _buildPaidOptions()
              : _buildNoItems(true),
          _buildRentButton()
        ],
      ),
    );
  }

  _buildRentButton() {
    final format = NumberFormat("### €");

    return Container(
      decoration: BoxDecoration(
          color: LocationStyle.backgroundColorPurple,
          borderRadius: BorderRadius.circular(8.0)),
      margin: const EdgeInsets.fromLTRB(8.0, 16.0, 8.0, 16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 8.8),
            child: Text(
              format.format(widget._habitation.pricePerMonth),
              style: LocationTextStyle.regularWhiteTextStyle
                  .copyWith(fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 8.8),
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            BookLocation(widget._habitation)));
              },
              child: const Text('Louer'),
            ),
          )
        ],
      ),
    );
  }

  _buildItems() {
    var width = (MediaQuery.of(context).size.width / 2) - 15;
    return Wrap(
      spacing: 2.0,
      children: Iterable.generate(
          widget._habitation.options.length,
          (i) => Container(
              height: 36,
              width: width,
              padding: const EdgeInsets.fromLTRB(15, 0, 0, 0),
              margin: const EdgeInsets.all(2),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(widget._habitation.options[i].name),
                  Text(
                    widget._habitation.options[i].description,
                    style: LocationTextStyle.regularGreyTextStyle,
                  )
                ],
              ))).toList(),
    );
  }

  _buildPaidOptions() {
    final format = NumberFormat("### €");
    var width = (MediaQuery.of(context).size.width / 2) - 15;
    return Wrap(
      spacing: 2.0,
      children: Iterable.generate(
          widget._habitation.paidOptions.length,
          (i) => Container(
              width: width,
              padding: const EdgeInsets.fromLTRB(15, 0, 0, 0),
              height: 36,
              margin: const EdgeInsets.all(2),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(widget._habitation.paidOptions[i].name),
                  Text(
                    format.format(widget._habitation.paidOptions[i].price),
                    style: LocationTextStyle.regularGreyTextStyle,
                  )
                ],
              ))).toList(),
    );
  }

  _buildNoItems(bool isPaidOption) {
    return Text('No ${isPaidOption ? 'paid options' : 'options'}.');
  }

  _buildCustomDivider(String text) {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
      child: Row(
        children: [
          Text(
            text,
            style: LocationTextStyle.subTitleboldTextStyle,
          ),
          const Expanded(
            child: Divider(
              height: 36,
              indent: 10,
              endIndent: 10,
              color: LocationStyle.colorPurple,
            ),
          ),
          const SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }
}
