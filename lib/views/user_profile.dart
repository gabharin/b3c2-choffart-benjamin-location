import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:location/cubit/user_cubit.dart';
import 'package:location/main.dart';
import 'package:location/views/login_page.dart';
import 'package:location/views/shared/bottom_navigation_bar_widget.dart';

class UserProfile extends StatefulWidget {
  static const String routeName = 'user_profile';

  const UserProfile({super.key});

  @override
  State<UserProfile> createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Mon profil')),
      bottomNavigationBar: const BottomNavigationBarWidget(3),
      body: BlocBuilder<UserCubit, UserState>(
        builder: (context, state) {
          if (state is UserLoaded) {
            return Container(
              margin: const EdgeInsets.all(24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'Prénom',
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                  Text(state.user.firstName,
                      style: const TextStyle(fontSize: 24)),
                  const SizedBox(
                    height: 16,
                  ),
                  const Text(
                    'Nom',
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                  Text(state.user.lastName,
                      style: const TextStyle(fontSize: 24)),
                  const SizedBox(
                    height: 16,
                  ),
                  const Text(
                    'E-mail',
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                  Text(state.user.email.toLowerCase(),
                      style: const TextStyle(fontSize: 24)),
                  const SizedBox(
                    height: 16,
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: ElevatedButton.icon(
                        onPressed: () {
                          context.read<UserCubit>().logOut();
                          Navigator.pushNamedAndRemoveUntil(
                              context, '/', (route) => false);
                        },
                        icon: const Icon(Icons.cancel),
                        label: const Text('Se déconnecter')),
                  ),
                ],
              ),
            );
          } else {
            return Text('NO USER???????');
          }
        },
      ),
    );
  }
}
