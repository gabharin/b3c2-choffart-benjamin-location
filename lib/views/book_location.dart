import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:location/models/habitation.dart';
import 'package:location/services/date_formater_service.dart';
import 'package:location/shared/location_style.dart';
import 'package:location/views/location_list.dart';
import 'package:location/views/shared/bottom_navigation_bar_widget.dart';
import '../shared/location_text_style.dart';

class BookLocation extends StatefulWidget {
  final Habitation _habitation;
  const BookLocation(this._habitation, {super.key});

  @override
  State<BookLocation> createState() => _BookLocationState();
}

class _BookLocationState extends State<BookLocation> {
  DateTime startDate = DateTime.now();
  DateTime endDate = DateTime.now();
  String maxCapacity = "1";
  List<CheckablePaidOption> checkablePaidOptions = [];

  var format = NumberFormat("### €");

  @override
  Widget build(BuildContext context) {
    _loadPaidOptions();
    return Scaffold(
      appBar: AppBar(title: const Text("Réservation")),
      bottomNavigationBar: const BottomNavigationBarWidget(1),
      body: ListView(
        padding: const EdgeInsets.all(4.0),
        children: [
          _buildSummary(),
          _buildDates(),
          _buildMaxCapacity(),
          checkablePaidOptions.isNotEmpty
              ? _buildPaidOptions(context)
              : const SizedBox(),
          _buildTotalWidget(),
          _buildRentButton()
        ],
      ),
    );
  }

  void _loadPaidOptions() {
    if (checkablePaidOptions.isEmpty &&
        widget._habitation.paidOptions.isNotEmpty) {
      checkablePaidOptions =
          widget._habitation.paidOptions.map<CheckablePaidOption>((po) {
        return CheckablePaidOption(po.id, po.name, false,
            description: po.description, price: po.price);
      }).toList();
    }
  }

  _buildSummary() {
    return ListTile(
      leading: const Icon(Icons.house),
      title: Text(widget._habitation.name),
      subtitle: Text(widget._habitation.address),
    );
  }

  _buildDates() {
    return Row(
      children: [
        _buildDateInput(startDate),
        const Expanded(
          flex: 2,
          child: CircleAvatar(
            backgroundColor: LocationStyle.backgroundColorPurple,
            child: Icon(Icons.arrow_forward),
          ),
        ),
        _buildDateInput(endDate)
      ],
    );
  }

  _buildDateInput(DateTime date) {
    return Expanded(
        flex: 5,
        child: GestureDetector(
          onTap: () => dateTimeRangePicker(),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Icon(
                Icons.calendar_month,
                color: LocationStyle.colorPurple,
              ),
              const SizedBox(
                width: 10,
              ),
              Text(
                DateFormaterService().formatDate(date),
                style: LocationTextStyle.subTitleboldTextStyle,
              )
            ],
          ),
        ));
  }

  dateTimeRangePicker() async {
    DateTimeRange? datePicked = await showDateRangePicker(
        context: context,
        firstDate: DateTime(DateTime.now().year),
        lastDate: DateTime(DateTime.now().year + 2),
        initialDateRange: DateTimeRange(start: startDate, end: endDate),
        cancelText: 'Annuler',
        confirmText: 'Valider',
        locale: const Locale("fr", "FR"));
    if (datePicked != null) {
      setState(() {
        startDate = datePicked.start;
        endDate = datePicked.end;
      });
    }
  }

  _buildMaxCapacity() {
    return Container(
      margin: const EdgeInsets.fromLTRB(20, 10, 0, 0),
      child: Row(
        children: [
          Text(
            'Nombre de personnes',
            style: LocationTextStyle.subTitleboldTextStyle,
          ),
          const SizedBox(
            width: 20,
          ),
          DropdownButton<String>(
              value: maxCapacity,
              items: List<String>.generate(8, (index) => (index + 1).toString())
                  .map((value) => DropdownMenuItem(
                        value: value,
                        child: Text(value),
                      ))
                  .toList(),
              onChanged: (value) {
                setState(() {
                  maxCapacity = value.toString();
                });
              })
        ],
      ),
    );
  }

  _buildPaidOptions(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: checkablePaidOptions.length,
      prototypeItem: ListTile(
        title: Text(checkablePaidOptions.first.name),
      ),
      itemBuilder: (context, index) {
        return CheckboxListTile(
          secondary: const Icon(Icons.add_shopping_cart),
          value: checkablePaidOptions[index].checked,
          title: Text(checkablePaidOptions[index].name),
          subtitle: Text(checkablePaidOptions[index].description),
          onChanged: (value) {
            setState(() {
              checkablePaidOptions[index].checked = value ?? true;
            });
          },
        );
      },
    );
  }

  _buildTotalWidget() {
    var total = widget._habitation.pricePerMonth;
    for (var option in checkablePaidOptions) {
      if (option.checked) {
        total += option.price;
      }
    }
    return Container(
      padding: const EdgeInsets.all(16.0),
      margin: const EdgeInsets.fromLTRB(8, 16, 8, 0),
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(8)),
          border: Border.all(color: LocationStyle.backgroundColorPurple)),
      child: Row(children: [
        Expanded(
          flex: 5,
          child: Text(
            'TOTAL',
            textAlign: TextAlign.center,
            style: LocationTextStyle.subTitleboldTextStyle,
          ),
        ),
        Expanded(
            flex: 1,
            child: Text(
              format.format(total),
              style: LocationTextStyle.subTitleboldTextStyle,
            ))
      ]),
    );
  }

  _buildRentButton() {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => LocationList()));
      },
      child: Container(
        padding: const EdgeInsets.all(16.0),
        margin: const EdgeInsets.fromLTRB(8, 16, 8, 0),
        decoration: const BoxDecoration(
            color: LocationStyle.backgroundColorPurple,
            borderRadius: BorderRadius.all(Radius.circular(8))),
        child: Row(children: [
          Expanded(
            child: Text(
              'Louer',
              textAlign: TextAlign.center,
              style:
                  LocationTextStyle.boldTextStyle.copyWith(color: Colors.white),
            ),
          ),
        ]),
      ),
    );
  }
}
