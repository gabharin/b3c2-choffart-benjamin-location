import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:location/models/bill.dart';
import 'package:location/services/date_formater_service.dart';
import 'package:location/services/habitation_service.dart';
import 'package:location/services/location_service.dart';
import 'package:location/views/shared/bottom_navigation_bar_widget.dart';

import '../models/location.dart';
import '../shared/location_style.dart';
import '../shared/location_text_style.dart';

class LocationList extends StatefulWidget {
  static const String routeName = 'location_list';

  const LocationList({super.key});

  @override
  State<LocationList> createState() => _LocationListState();
}

class _LocationListState extends State<LocationList> {
  final LocationService locationService = LocationService();
  late Future<List<Location>> _locations;

  @override
  void initState() {
    super.initState();
    _locations = locationService.getLocations();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Mes locations')),
        bottomNavigationBar: const BottomNavigationBarWidget(2),
        body: FutureBuilder(
            future: _locations,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                final responseData = snapshot.data;
                if (responseData != null) {
                  return ListView.builder(
                      itemCount: responseData.length,
                      itemBuilder: ((context, index) => _buildLocationTile(
                          context, responseData.toList()[index])));
                } else {
                  return const Center(
                    child: Text('Aucune réservation.'),
                  );
                }
              } else {
                return const SpinKitHourGlass(color: LocationStyle.colorPurple);
              }
            }));
  }

  _buildLocationTile(BuildContext context, Location location) {
    var format = NumberFormat("### €");
    var habitation =
        HabitationService().getSpecificHabitations(location.habitationId);
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            title: Text(habitation.name),
            subtitle: Text(habitation.address),
            trailing: Text(
              format.format(location.totalPrice),
              style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          _buildDates(location.startDate, location.endDate),
          const SizedBox(
            height: 20,
          ),
          _buildBill(location.bill)
        ],
      ),
    );
  }

  _buildDates(DateTime startDate, DateTime endDate) {
    return Row(
      children: [
        _buildDateInput(startDate),
        const Expanded(
          flex: 2,
          child: CircleAvatar(
            backgroundColor: LocationStyle.backgroundColorPurple,
            child: Icon(Icons.arrow_forward),
          ),
        ),
        _buildDateInput(endDate)
      ],
    );
  }

  _buildDateInput(DateTime date) {
    return Expanded(
        flex: 5,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(
              Icons.calendar_month,
              color: LocationStyle.colorPurple,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              DateFormaterService().formatDate(date),
              style: LocationTextStyle.subTitleboldTextStyle,
            )
          ],
        ));
  }

  _buildBill(Bill? bill) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 0, 0, 0),
      child: Text(bill is Bill
          ? 'Facture délivrée le ${DateFormaterService().formatDate(bill.date)}'
          : 'Aucune facture.'),
    );
  }
}
