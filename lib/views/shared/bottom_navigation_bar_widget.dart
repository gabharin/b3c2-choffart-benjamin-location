import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:location/cubit/user_cubit.dart';
import 'package:location/views/location_list.dart';
import 'package:location/views/login_page.dart';
import 'package:location/views/user_profile.dart';

import 'badge_widget.dart';

class BottomNavigationBarWidget extends StatelessWidget {
  final int indexSelected;

  const BottomNavigationBarWidget(this.indexSelected, {super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserCubit, UserState>(
      builder: (context, state) {
        var isUserNotConnected = state is UserInitial;
        return BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: indexSelected,
          items: <BottomNavigationBarItem>[
            const BottomNavigationBarItem(
                icon: Icon(Icons.home), label: 'Accueil'),
            const BottomNavigationBarItem(
                icon: Icon(Icons.search), label: 'Recherche'),
            BottomNavigationBarItem(
                icon: isUserNotConnected
                    ? const Icon(Icons.shopping_bag_outlined)
                    : const BadgeWidget(
                        value: 0,
                        top: 0,
                        right: 0,
                        child: Icon(Icons.shopping_cart)),
                label: 'Locations'),
            BottomNavigationBarItem(
                icon: Icon(isUserNotConnected ? Icons.login : Icons.person),
                label: isUserNotConnected ? 'Se connecter' : 'Profil'),
          ],
          onTap: (index) {
            String page = '/';
            switch (index) {
              case 2:
                page = isUserNotConnected
                    ? LoginPage.routeName
                    : LocationList.routeName;
                break;
              case 3:
                page = isUserNotConnected
                    ? LoginPage.routeName
                    : UserProfile.routeName;
                break;
            }
            Navigator.pushNamedAndRemoveUntil(context, page, (route) => false);
          },
        );
      },
    );
  }
}
