import 'package:flutter/material.dart';
import 'package:location/models/habitation.dart';
import 'package:location/views/shared/habitation_option.dart';

class HabitationFeaturesWidget extends StatelessWidget {
  final Habitation _habitation;

  const HabitationFeaturesWidget(this._habitation, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        HabitationOption(Icons.group, "${_habitation.maxCapacity} personnes"),
        HabitationOption(Icons.bed, "${_habitation.rooms} chambres"),
        HabitationOption(Icons.fit_screen, "${_habitation.surface} m²")
      ],
    );
  }
}
