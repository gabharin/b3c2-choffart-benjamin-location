class Bill {
  int id;
  DateTime date;
  String address;

  Bill(this.id, this.date, {this.address = ""});
  Bill.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        date = DateTime.parse(json['date']),
        address = json['address'];
}
