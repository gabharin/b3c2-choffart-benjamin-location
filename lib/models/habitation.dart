import 'package:location/models/space_type.dart';

class Habitation {
  int id;
  SpaceType spaceType;
  String image;
  String name;
  String address;
  int maxCapacity;
  int rooms;
  int surface;
  double pricePerMonth;
  int beds;
  int bathRooms;
  List<Option> options;
  List<PaidOption> paidOptions;

  Habitation(
      this.id,
      this.spaceType,
      this.image,
      this.name,
      this.address,
      this.maxCapacity,
      this.rooms,
      this.surface,
      this.pricePerMonth,
      this.beds,
      this.bathRooms,
      {this.options = const [],
      this.paidOptions = const []});
  Habitation.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        spaceType = SpaceType.fromJson(json['spaceType']),
        image = json['image'],
        name = json['name'],
        address = json['address'],
        maxCapacity = json['maxCapacity'],
        rooms = json['rooms'],
        beds = json['beds'],
        bathRooms = json['bathRooms'],
        surface = json['surface'],
        pricePerMonth = json['pricePerMonth'],
        options = (json['items'] as List)
            .map((item) => Option.fromJson(item))
            .toList(),
        paidOptions = (json['paidOptions'] as List).map((item) {
          return PaidOption.fromJson({
            "id": item['paidOption']['id'],
            "name": item['paidOption']['name'],
            "description": item['paidOption']['description'],
            "price": item['price']
          });
        }).toList();
}

class Option {
  dynamic id;
  String name;
  String description;

  Option(this.id, this.name, {this.description = ""});
  Option.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        description = json['description'];
}

class PaidOption extends Option {
  double price;

  PaidOption(super.id, super.name, {super.description = "", this.price = 0});
  PaidOption.fromJson(Map<String, dynamic> json)
      : price = json['price'],
        super.fromJson(json);
}

class CheckablePaidOption extends PaidOption {
  bool checked;

  CheckablePaidOption(super.id, super.name, this.checked,
      {super.description = "", super.price});
}
