import 'package:location/models/bill.dart';

import 'habitation.dart';

class Location {
  int id;
  int habitationId;
  DateTime startDate;
  DateTime endDate;
  double totalPrice;
  double totalSpent;
  Bill? bill;

  Habitation? habitation;
  List<PaidOption> paidOptions;

  Location(this.id, this.habitationId, this.startDate, this.endDate,
      this.totalPrice, this.totalSpent,
      {this.bill, this.habitation, this.paidOptions = const []});

  Location.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        habitationId = json['habitationId'],
        startDate = DateTime.parse(json['startDate']),
        endDate = DateTime.parse(json['endDate']),
        totalPrice = json['totalPrice'],
        totalSpent = json['totalSpent'],
        bill = json['bill'] == null ? null : Bill.fromJson(json['bill']),
        paidOptions = (json['locationPaidOptions'] as List).map((item) {
          return PaidOption.fromJson({
            "id": item['paidOption']['id'],
            "name": item['paidOption']['name'],
            "description": item['paidOption']['description'],
            "price": item['price']
          });
        }).toList();
}
