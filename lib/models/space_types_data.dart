import 'package:location/models/space_type.dart';

class SpaceTypesData {
  static final data = [
    {"id": 1, "name": "Maison"},
    {"id": 2, "name": "Appartement"}
  ];

  static List<SpaceType> buildList() {
    return data.map((item) => SpaceType.fromJson(item)).toList();
  }
}
