class SpaceType {
  int id;
  String name;

  SpaceType(this.id, this.name);
  SpaceType.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'];
}
