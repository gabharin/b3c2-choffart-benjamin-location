class User {
  int id;
  String email;
  String password;
  String firstName;
  String lastName;

  User(this.id, this.email, this.password, this.firstName, this.lastName);
}
