import 'location.dart';

class LocationsData {
  static final data = [
    {
      "id": 1,
      "userId": 6,
      "habitationId": 1,
      "startDate": "2022-07-01T00:00:00",
      "endDate": "2022-07-04T00:00:00",
      "totalPrice": 710.0,
      "totalSpent": 0.0,
      "bill": {
        "id": 1,
        "date": "2022-08-24T00:00:00",
        "address": "address",
      },
      "locationPaidOptions": [
        {
          "locationId": 1,
          "paidOptionId": 1,
          "price": 60.0,
          "paidOption": {
            "id": 1,
            "name": "Ménage",
            "description": "A la fin du séjour"
          }
        },
        {
          "locationId": 1,
          "paidOptionId": 2,
          "price": 30.0,
          "paidOption": {
            "id": 2,
            "name": "Drap de lit",
            "description": "Pour l'ensemble des lits"
          }
        },
        {
          "locationId": 1,
          "paidOptionId": 3,
          "price": 20.0,
          "paidOption": {
            "id": 3,
            "name": "Linge de maison",
            "description": "Linge de toilette pour la salle de bain"
          }
        }
      ],
      "payments": [
        {
          "id": 4,
          "locationId": 1,
          "amount": 200.0,
          "paymentDate": "2022-07-04T11:01:31",
          "paymentTypeId": 1,
          "location": null,
          "paymentType": {
            "id": 1,
            "name": "Carte Bleue",
            "payments": [
              null,
              {
                "id": 5,
                "locationId": 1,
                "amount": 200.0,
                "paymentDate": "2022-07-04T11:01:52",
                "paymentTypeId": 1,
                "location": null,
                "paymentType": null
              },
              {
                "id": 6,
                "locationId": 1,
                "amount": 200.0,
                "paymentDate": "2022-07-04T11:01:57",
                "paymentTypeId": 1,
                "location": null,
                "paymentType": null
              }
            ]
          }
        },
        {
          "id": 5,
          "locationId": 1,
          "amount": 200.0,
          "paymentDate": "2022-07-04T11:01:52",
          "paymentTypeId": 1,
          "location": null,
          "paymentType": {
            "id": 1,
            "name": "Carte Bleue",
            "payments": [
              {
                "id": 4,
                "locationId": 1,
                "amount": 200.0,
                "paymentDate": "2022-07-04T11:01:31",
                "paymentTypeId": 1,
                "location": null,
                "paymentType": null
              },
              null,
              {
                "id": 6,
                "locationId": 1,
                "amount": 200.0,
                "paymentDate": "2022-07-04T11:01:57",
                "paymentTypeId": 1,
                "location": null,
                "paymentType": null
              }
            ]
          }
        },
        {
          "id": 6,
          "locationId": 1,
          "amount": 200.0,
          "paymentDate": "2022-07-04T11:01:57",
          "paymentTypeId": 1,
          "location": null,
          "paymentType": {
            "id": 1,
            "name": "Carte Bleue",
            "payments": [
              {
                "id": 4,
                "locationId": 1,
                "amount": 200.0,
                "paymentDate": "2022-07-04T11:01:31",
                "paymentTypeId": 1,
                "location": null,
                "paymentType": null
              },
              {
                "id": 5,
                "locationId": 1,
                "amount": 200.0,
                "paymentDate": "2022-07-04T11:01:52",
                "paymentTypeId": 1,
                "location": null,
                "paymentType": null
              },
              null
            ]
          }
        }
      ],
      "reminders": [
        {
          "id": 1,
          "locationId": 1,
          "date": "2022-06-24T16:04:05",
          "motive": "ee",
          "location": null
        }
      ]
    },
    {
      "id": 2,
      "userId": 6,
      "habitationId": 2,
      "startDate": "2022-07-05T00:00:00",
      "endDate": "2022-07-07T00:00:00",
      "totalPrice": 40.0,
      "totalSpent": 0.0,
      "bill": null,
      "locationPaidOptions": [
        {
          "locationId": 2,
          "paidOptionId": 1,
          "price": 40.0,
          "paidOption": {
            "id": 1,
            "name": "Ménage",
            "description": "A la fin du séjour"
          }
        }
      ],
      "payments": [],
      "reminders": []
    }
  ];

  static List<Location> buildList() {
    return data.map((item) => Location.fromJson(item)).toList();
  }
}
