import 'habitation.dart';

class HabitationsData {
  static final data = [
    {
      "id": 1,
      "spaceType": {"id": 1, "name": "Maison"},
      "items": [
        {"id": 2, "name": "Lac", "description": "Base de loisirs à 2 km"},
        {"id": 1, "name": "Internet", "description": "Wifi et Fibre"}
      ],
      "paidOptions": [
        {
          "id": {"habitationId": 1, "paidOptionId": 2},
          "paidOption": {
            "id": 2,
            "name": "Drap de lit",
            "description": "Pour l'ensemble des beds"
          },
          "price": 30.0
        },
        {
          "id": {"habitationId": 1, "paidOptionId": 3},
          "paidOption": {
            "id": 3,
            "name": "Linge de maison",
            "description": "Linge de toilette pour la salle de bain"
          },
          "price": 20.0
        },
        {
          "id": {"habitationId": 1, "paidOptionId": 1},
          "paidOption": {
            "id": 1,
            "name": "Ménage",
            "description": "A la fin du séjour"
          },
          "price": 60.0
        }
      ],
      "name": "Maison provençale",
      "description": "Une description",
      "address": "12 Rue du Coq qui chante",
      "idVille": 56,
      "image": "maison.png",
      "maxCapacity": 8,
      "rooms": 3,
      "beds": 3,
      "bathRooms": 1,
      "surface": 92,
      "pricePerMonth": 600.0
    },
    {
      "id": 2,
      "spaceType": {"id": 2, "name": "Appartement"},
      "items": [
        {"id": 1, "name": "Internet", "description": "Wifi et Fibre"}
      ],
      "paidOptions": [
        {
          "id": {"habitationId": 2, "paidOptionId": 1},
          "paidOption": {
            "id": 1,
            "name": "Ménage",
            "description": "A la fin du séjour"
          },
          "price": 40.0
        }
      ],
      "name": "Appartement centre ville",
      "description": "bla bla",
      "address": "Rue du centre",
      "idVille": 99,
      "image": "appartement.png",
      "maxCapacity": 4,
      "rooms": 1,
      "beds": 2,
      "bathRooms": 1,
      "surface": 50,
      "pricePerMonth": 555.0
    },
    {
      "id": 3,
      "spaceType": {"id": 1, "name": "Maison"},
      "items": [],
      "paidOptions": [],
      "name": "Maison 3",
      "description": "Desc Maison 3",
      "address": "Rue 3",
      "idVille": 4,
      "image": "maison.png",
      "maxCapacity": 5,
      "rooms": 2,
      "beds": 3,
      "bathRooms": 1,
      "surface": 100,
      "pricePerMonth": 500.0
    },
    {
      "id": 4,
      "spaceType": {"id": 1, "name": "Maison"},
      "items": [
        {
          "id": 3,
          "name": "Climatisation",
          "description": "Climatisation réversible"
        }
      ],
      "paidOptions": [],
      "name": "Maison 4",
      "description": "Desc Maison 4",
      "address": "Rue 4",
      "idVille": 4,
      "image": "maison.png",
      "maxCapacity": 5,
      "rooms": 2,
      "beds": 3,
      "bathRooms": 1,
      "surface": 100,
      "pricePerMonth": 500.0
    },
    {
      "id": 5,
      "spaceType": {"id": 1, "name": "Maison"},
      "items": [],
      "paidOptions": [
        {
          "id": {"habitationId": 5, "paidOptionId": 1},
          "paidOption": {
            "id": 1,
            "name": "Ménage",
            "description": "A la fin du séjour"
          },
          "price": 50.0
        },
        {
          "id": {"habitationId": 5, "paidOptionId": 2},
          "paidOption": {
            "id": 2,
            "name": "Drap de lit",
            "description": "Pour l'ensemble des beds"
          },
          "price": 40.0
        }
      ],
      "name": "Maison 5",
      "description": "Desc Maison 5",
      "address": "Rue 5",
      "idVille": 4,
      "image": "maison.png",
      "maxCapacity": 6,
      "rooms": 3,
      "beds": 4,
      "bathRooms": 1,
      "surface": 120,
      "pricePerMonth": 600.0
    },
    {
      "id": 6,
      "spaceType": {"id": 1, "name": "Maison"},
      "items": [],
      "paidOptions": [],
      "name": "Maison 6",
      "description": "Desc Maison 6",
      "address": "Rue 6",
      "idVille": 4,
      "image": "maison.png",
      "maxCapacity": 5,
      "rooms": 2,
      "beds": 3,
      "bathRooms": 1,
      "surface": 100,
      "pricePerMonth": 500.0
    },
    {
      "id": 7,
      "spaceType": {"id": 1, "name": "Maison"},
      "items": [
        {
          "id": 3,
          "name": "Climatisation",
          "description": "Climatisation réversible"
        }
      ],
      "paidOptions": [],
      "name": "Maison 7",
      "description": "Desc Maison 7",
      "address": "Rue 7",
      "idVille": 4,
      "image": "maison.png",
      "maxCapacity": 6,
      "rooms": 3,
      "beds": 4,
      "bathRooms": 1,
      "surface": 120,
      "pricePerMonth": 600.0
    },
    {
      "id": 8,
      "spaceType": {"id": 1, "name": "Maison"},
      "items": [],
      "paidOptions": [],
      "name": "Maison 8",
      "description": "Desc Maison 8",
      "address": "Rue 8",
      "idVille": 4,
      "image": "maison.png",
      "maxCapacity": 5,
      "rooms": 2,
      "beds": 3,
      "bathRooms": 1,
      "surface": 100,
      "pricePerMonth": 500.0
    },
    {
      "id": 9,
      "spaceType": {"id": 1, "name": "Maison"},
      "items": [],
      "paidOptions": [],
      "name": "Maison 9",
      "description": "Desc Maison 9",
      "address": "Rue 9",
      "idVille": 4,
      "image": "maison.png",
      "maxCapacity": 6,
      "rooms": 3,
      "beds": 4,
      "bathRooms": 1,
      "surface": 120,
      "pricePerMonth": 600.0
    },
    {
      "id": 10,
      "spaceType": {"id": 1, "name": "Maison"},
      "items": [
        {
          "id": 3,
          "name": "Climatisation",
          "description": "Climatisation réversible"
        }
      ],
      "paidOptions": [],
      "name": "Maison 10",
      "description": "Desc Maison 10",
      "address": "Rue 10",
      "idVille": 4,
      "image": "maison.png",
      "maxCapacity": 5,
      "rooms": 2,
      "beds": 3,
      "bathRooms": 1,
      "surface": 100,
      "pricePerMonth": 500.0
    },
    {
      "id": 11,
      "spaceType": {"id": 2, "name": "Appartement"},
      "items": [
        {
          "id": 3,
          "name": "Climatisation",
          "description": "Climatisation réversible"
        }
      ],
      "paidOptions": [
        {
          "id": {"habitationId": 11, "paidOptionId": 1},
          "paidOption": {
            "id": 1,
            "name": "Ménage",
            "description": "A la fin du séjour"
          },
          "price": 40.0
        }
      ],
      "name": "Appartement 4",
      "description": "Desc Appartement 4",
      "address": "Rue 4",
      "idVille": 3,
      "image": "appartement.png",
      "maxCapacity": 5,
      "rooms": 2,
      "beds": 3,
      "bathRooms": 1,
      "surface": 70,
      "pricePerMonth": 350.0
    },
    {
      "id": 12,
      "spaceType": {"id": 2, "name": "Appartement"},
      "items": [
        {
          "id": 3,
          "name": "Climatisation",
          "description": "Climatisation réversible"
        }
      ],
      "paidOptions": [
        {
          "id": {"habitationId": 12, "paidOptionId": 1},
          "paidOption": {
            "id": 1,
            "name": "Ménage",
            "description": "A la fin du séjour"
          },
          "price": 50.0
        },
        {
          "id": {"habitationId": 12, "paidOptionId": 2},
          "paidOption": {
            "id": 2,
            "name": "Drap de lit",
            "description": "Pour l'ensemble des beds"
          },
          "price": 20.0
        }
      ],
      "name": "Appartement 5",
      "description": "Desc Appartement 5",
      "address": "Rue 5",
      "idVille": 3,
      "image": "appartement.png",
      "maxCapacity": 6,
      "rooms": 3,
      "beds": 3,
      "bathRooms": 1,
      "surface": 80,
      "pricePerMonth": 400.0
    },
    {
      "id": 13,
      "spaceType": {"id": 2, "name": "Appartement"},
      "items": [
        {
          "id": 3,
          "name": "Climatisation",
          "description": "Climatisation réversible"
        }
      ],
      "paidOptions": [
        {
          "id": {"habitationId": 13, "paidOptionId": 1},
          "paidOption": {
            "id": 1,
            "name": "Ménage",
            "description": "A la fin du séjour"
          },
          "price": 40.0
        },
        {
          "id": {"habitationId": 13, "paidOptionId": 2},
          "paidOption": {
            "id": 2,
            "name": "Drap de lit",
            "description": "Pour l'ensemble des beds"
          },
          "price": 30.0
        }
      ],
      "name": "Appartement 6",
      "description": "Desc Appartement 6",
      "address": "Rue 6",
      "idVille": 3,
      "image": "appartement.png",
      "maxCapacity": 5,
      "rooms": 2,
      "beds": 3,
      "bathRooms": 1,
      "surface": 70,
      "pricePerMonth": 350.0
    },
    {
      "id": 14,
      "spaceType": {"id": 2, "name": "Appartement"},
      "items": [
        {
          "id": 3,
          "name": "Climatisation",
          "description": "Climatisation réversible"
        }
      ],
      "paidOptions": [
        {
          "id": {"habitationId": 14, "paidOptionId": 1},
          "paidOption": {
            "id": 1,
            "name": "Ménage",
            "description": "A la fin du séjour"
          },
          "price": 50.0
        },
        {
          "id": {"habitationId": 14, "paidOptionId": 2},
          "paidOption": {
            "id": 2,
            "name": "Drap de lit",
            "description": "Pour l'ensemble des beds"
          },
          "price": 20.0
        }
      ],
      "name": "Appartement 7",
      "description": "Desc Appartement 7",
      "address": "Rue 7",
      "idVille": 3,
      "image": "appartement.png",
      "maxCapacity": 6,
      "rooms": 3,
      "beds": 3,
      "bathRooms": 1,
      "surface": 80,
      "pricePerMonth": 400.0
    },
    {
      "id": 15,
      "spaceType": {"id": 2, "name": "Appartement"},
      "items": [
        {
          "id": 3,
          "name": "Climatisation",
          "description": "Climatisation réversible"
        }
      ],
      "paidOptions": [
        {
          "id": {"habitationId": 15, "paidOptionId": 2},
          "paidOption": {
            "id": 2,
            "name": "Drap de lit",
            "description": "Pour l'ensemble des beds"
          },
          "price": 30.0
        },
        {
          "id": {"habitationId": 15, "paidOptionId": 1},
          "paidOption": {
            "id": 1,
            "name": "Ménage",
            "description": "A la fin du séjour"
          },
          "price": 40.0
        }
      ],
      "name": "Appartement 8",
      "description": "Desc Appartement 8",
      "address": "Rue 8",
      "idVille": 3,
      "image": "appartement.png",
      "maxCapacity": 5,
      "rooms": 2,
      "beds": 3,
      "bathRooms": 1,
      "surface": 70,
      "pricePerMonth": 350.0
    },
    {
      "id": 16,
      "spaceType": {"id": 2, "name": "Appartement"},
      "items": [
        {
          "id": 3,
          "name": "Climatisation",
          "description": "Climatisation réversible"
        }
      ],
      "paidOptions": [
        {
          "id": {"habitationId": 16, "paidOptionId": 1},
          "paidOption": {
            "id": 1,
            "name": "Ménage",
            "description": "A la fin du séjour"
          },
          "price": 50.0
        },
        {
          "id": {"habitationId": 16, "paidOptionId": 2},
          "paidOption": {
            "id": 2,
            "name": "Drap de lit",
            "description": "Pour l'ensemble des beds"
          },
          "price": 20.0
        }
      ],
      "name": "Appartement 9",
      "description": "Desc Appartement 9",
      "address": "Rue 9",
      "idVille": 3,
      "image": "appartement.png",
      "maxCapacity": 6,
      "rooms": 3,
      "beds": 3,
      "bathRooms": 1,
      "surface": 80,
      "pricePerMonth": 400.0
    },
    {
      "id": 17,
      "spaceType": {"id": 1, "name": "Maison"},
      "items": [
        {
          "id": 3,
          "name": "Climatisation",
          "description": "Climatisation réversible"
        }
      ],
      "paidOptions": [
        {
          "id": {"habitationId": 17, "paidOptionId": 1},
          "paidOption": {
            "id": 1,
            "name": "Ménage",
            "description": "A la fin du séjour"
          },
          "price": 40.0
        },
        {
          "id": {"habitationId": 17, "paidOptionId": 2},
          "paidOption": {
            "id": 2,
            "name": "Drap de lit",
            "description": "Pour l'ensemble des beds"
          },
          "price": 30.0
        }
      ],
      "name": "Appartement 10",
      "description": "Desc Appartement 10",
      "address": "Rue 10",
      "idVille": 3,
      "image": "appartement.png",
      "maxCapacity": 5,
      "rooms": 2,
      "beds": 3,
      "bathRooms": 1,
      "surface": 70,
      "pricePerMonth": 350.0
    }
  ];

  static List<Habitation> buildList() {
    List<Habitation> list =
        data.map((item) => Habitation.fromJson(item)).toList();
    return list;
  }
}
