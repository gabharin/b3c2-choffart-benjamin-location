import 'package:location/models/location_api_client.dart';
import 'package:location/models/locations_data.dart';

import 'location.dart';

class LocationApiData implements LocationApiClient {
  @override
  Future<List<Location>> getLocations() {
    return Future.delayed(
        const Duration(seconds: 1), () => LocationsData.buildList());
  }

  @override
  Future<Location> getLocation(int id) {
    Location location =
        LocationsData.buildList().where((l) => l.id == id).first;
    return Future.delayed(const Duration(seconds: 1), () => location);
  }
}
