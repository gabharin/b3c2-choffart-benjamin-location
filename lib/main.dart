import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart';
import 'package:location/models/habitation.dart';
import 'package:location/models/space_type.dart';
import 'package:location/services/habitation_service.dart';
import 'package:location/shared/location_style.dart';
import 'package:location/shared/location_text_style.dart';
import 'package:location/views/habitation_details.dart';
import 'package:location/views/habitation_list.dart';
import 'package:location/views/location_list.dart';
import 'package:location/views/login_page.dart';
import 'package:location/views/shared/bottom_navigation_bar_widget.dart';
import 'package:location/views/user_profile.dart';
import 'package:location/views/validation_location.dart';

import 'cubit/user_cubit.dart';
import 'models/user.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => UserCubit(),
      child: MaterialApp(
        title: 'Location',
        theme: ThemeData(
          primarySwatch: LocationStyle.primarySwatch,
        ),
        home: MyHomePage(title: 'Mes locations'),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate],
        supportedLocales: const [Locale('en'), Locale('fr')],
        routes: {
          UserProfile.routeName: (context) => const UserProfile(),
          LoginPage.routeName: (context) => const LoginPage('/'),
          LocationList.routeName: (context) => const LocationList(),
          ValidationLocation.routeName: (context) => const ValidationLocation(),
        },
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final HabitationService habitationService = HabitationService();
  final String title;
  late List<SpaceType> _spaceTypes;
  late List<Habitation> _habitations;
  MyHomePage({required this.title, super.key}) {
    _spaceTypes = habitationService.getSpaceTypes();
    _habitations = habitationService.getTop10Habitations();
  }

  _buildSpaceTypeList(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(6.0),
      height: 100,
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: List.generate(_spaceTypes.length,
              (index) => _buildSpaceType(context, _spaceTypes[index]))),
    );
  }

  _buildLastHabitation(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
      height: 240,
      child: ListView.builder(
          itemCount: _habitations.length,
          itemExtent: 220,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) =>
              _buildRow(_habitations[index], context)),
    );
  }

  _buildRow(Habitation habitation, BuildContext context) {
    var format = NumberFormat("### €");

    return Container(
      width: 240,
      margin: const EdgeInsets.all(4.0),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => HabitationDetails(habitation)));
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: Image.asset(
                'assets/images/locations/${habitation.image}',
                fit: BoxFit.fitWidth,
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Text(
              habitation.name,
              style: LocationTextStyle.regularTextStyle,
            ),
            Row(
              children: [
                const Icon(Icons.location_on_outlined),
                Text(
                  habitation.address,
                  style: LocationTextStyle.regularTextStyle,
                ),
              ],
            ),
            Text(format.format(habitation.pricePerMonth))
          ],
        ),
      ),
    );
  }

  _buildSpaceType(BuildContext context, SpaceType spaceType) {
    var icon = Icons.home;
    switch (spaceType.id) {
      case 1:
        icon = Icons.house;
        break;
      case 2:
        icon = Icons.apartment;
        break;
      default:
        icon = Icons.home;
    }
    return Expanded(
      child: Container(
        height: 80,
        decoration: BoxDecoration(
            color: LocationStyle.backgroundColorPurple,
            borderRadius: BorderRadius.circular(8.0)),
        margin: const EdgeInsets.all(8.0),
        child: InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => HabitationList(spaceType.id == 1)));
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(icon, color: Colors.white70),
              const SizedBox(width: 5),
              Text(
                spaceType.name,
                style: LocationTextStyle.regularWhiteTextStyle,
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      bottomNavigationBar: const BottomNavigationBarWidget(0),
      body: Center(
        child: Column(
          children: [
            const SizedBox(
              height: 30,
            ),
            _buildSpaceTypeList(context),
            const SizedBox(
              height: 20,
            ),
            _buildLastHabitation(context)
          ],
        ),
      ),
    );
  }
}
