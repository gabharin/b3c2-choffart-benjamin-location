class DateFormaterService {
  formatDate(DateTime date) {
    List<String> months = [
      'Janvier',
      'Février',
      'Mars',
      'Avril',
      'Mai',
      'Juin',
      'Juillet',
      'Août',
      'Septembre',
      'Octobre',
      'Novembre',
      'Décembre'
    ];
    var month = months[date.month - 1].length > 4
        ? "${months[date.month - 1].substring(0, 1).toLowerCase()}${months[date.month - 1].substring(1, 3)}."
        : "${months[date.month - 1].substring(0, 1).toLowerCase()}${months[date.month - 1].substring(1, months[date.month - 1].length)}";
    return "${date.day.toString()} $month ${date.year.toString()}";
  }
}
