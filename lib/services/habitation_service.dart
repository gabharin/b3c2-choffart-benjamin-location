import 'package:location/models/habitation.dart';
import 'package:location/models/habitations_data.dart';
import 'package:location/models/space_type.dart';
import 'package:location/models/space_types_data.dart';

class HabitationService {
  var _spaceTypes;
  var _habitations;

  HabitationService() {
    _spaceTypes = SpaceTypesData.buildList();
    _habitations = HabitationsData.buildList();
  }

  List<SpaceType> getSpaceTypes() {
    return _spaceTypes;
  }

  List<Habitation> _getHabitations({bool isHouse = true}) {
    return _habitations
        .where((element) => element.spaceType.id == (isHouse ? 1 : 2))
        .toList();
  }

  Habitation getSpecificHabitations(int id) {
    return _habitations.firstWhere((element) => element.id == id);
  }

  double getPriceForHabitationWithOptions(Habitation h) {
    var total = h.pricePerMonth;
    for (var paidOption in h.paidOptions) {
      total += paidOption.price;
    }
    return total;
  }

  List<Habitation> getTop10Habitations() {
    return _habitations
        .where((element) => element.id % 2 == 1)
        .take(10)
        .toList();
  }

  List<Habitation> getHouses() {
    return _getHabitations(isHouse: true);
  }

  List<Habitation> getApartments() {
    return _getHabitations(isHouse: false);
  }
}
