import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../models/user.dart';

part 'user_state.dart';

class UserCubit extends Cubit<UserState> {
  UserCubit() : super(const UserInitial());

  setUser(String email, String password, String firstName, String lastName) {
    emit(UserLoaded(User(1, email, password, firstName, lastName)));
  }

  logOut() {
    emit(const UserInitial());
  }
}
